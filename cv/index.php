<head>
    <?php
    //require "dev-tools.php"; // Uncomment for DEV
    ?>

    <link rel="stylesheet" href="../assets/css/no-printer-margin.css">
    <link rel="stylesheet" href="style.css">
    <script src="print.js"></script>
</head>

<body>

<div id="sidebar" class="column">
    <img src="assets/photo.png" id="photo">

    <hr>

    <div class="header">Contact</div>

    <p class="contacts">
        <img src="../assets/icons/email.png" class="icon">
        <span style="font-size: 18px;">philipp.nikolajev@gmail.com</span>
    </p>

    <p class="contacts">
        <img src="../assets/icons/phone.png" class="icon">
        <span>+372 56 278 748</span>
    </p>

    <br>

    <hr>

    <div class="header">Date of birth<span></span></div>

    <p class="date-of-birth">2nd April 1992</p>

    <br>

    <hr>

    <div class="header">Personal skills<span></span></div>

    <div class="personal-skills">
        <p>Strong verbal communication</p>
        <p>Community activities</p>
        <p>Stress resistance</p>
        <p>Goal orientation</p>
        <p>Creativity</p>
        <p>Attention to details</p>
    </div>

    <br>

    <hr>

    <div class="header">Language skills<span></span></div>

    <p class="language-skills">
        English
        <span>fluent</span>
    </p>

    <p class="language-skills">
        Russian
        <span>native</span>
    </p>

    <p class="language-skills">
        Estonian
        <span>fluent</span>
    </p>

    <br>

    <hr>

    <div class="header">Motto</div>

    <p class="text">
        <strong>DRY</strong> (Don't repeat yourself) a third time
        <span></span>
    </p>

    <br>

    <hr>

    <div class="header">Education</div>

    <p class="text education">
        <strong>The University of Tallinn</strong>
        <span></span>
        <br>
        The English Language and Culture
        <span></span>
    </p>

</div>


<div id="page" class="column">

    <h1>
        Philipp Nikolajev
    </h1>
    <h2>Senior full-stack web developer/ Team lead</h2>
    <hr>

    <h3>Professional summary</h3>

    <p>
        Currently involved mostly in <strong>back-end developing:</strong><br>
        3rd-party API integrations, API creation,
        database architecture, data analysis,
        maintenance script automation,
        developing from-scratch PHP apps using modern frameworks (Symfony 5+) with a focus on architecture,
        supporting and updating legacy projects (older than 2015)<br><br>

        <strong>Front-end developing:</strong><br>
        Back-end API driven Vue pages, jQuery scripts & plugins, HTMX, Alpine.js, NPM, Gulp, Babel ...<br><br>
        basic HTML & CSS skills (Tailwind, Twitter Bootstrap, LESS, SASS)<br><br>

        <strong>Familiar with the basics of:</strong><br>
        DevOps & SysAdmin on Linux, TypeScript, Node.js
        <br><br>

        Worked both in a team and independently, fully remote
    </p>

    <hr>

    <h3>Experience</h3>

    <p>

    <div class="workplace-header">
        <strong>WebExpert OÜ</strong> Estonia - <span class="position">Senior full-stack web developer/Team lead</span>
        <span class="time-period">2019 - Current</span>
    </div>

    <ul>
        <li>Symfony 2 (legacy project) & 5 (self-developed new project). Grand refactoring of third-party legacy project (Moving to the latest Symfony)</li>
        <li>
            more than 5 years old company-developed framework<br>
            (both supporting and developing new features)
        </li>
        <li>MySQL & PostgreSQL</li>
        <li>Vue.js, HTMX, Alpine.js & jQuery, Vanilla JS (ECMA 5-6), Tailwind</li>
        <li>ElasticSearch</li>
        <li>Basic server administration on Linux (docker, bash, cron, etc.)</li>
    </ul>

    </p>



    <p>

    <div class="workplace-header">
        <strong>Axactor Finland OY</strong> Finland - <span class="position">Data analytics</span>
        <span class="time-period">2021</span>
    </div>

    <ul>
        <li>Data migration from old legacy software (PHP & MySQL)</li>
    </ul>

    </p>

    <p>

    <div class="workplace-header">
        <strong>SPT Inkasso OÜ</strong> Estonia - <span class="position">Full-stack web developer</span>
        <span class="time-period">2017 - 2019</span>
        <br>
        <small><strong>Suomen Perintätoimisto OY</strong> Finland</small>

    </div>

    <ul>
        <li>CakePHP 2 (legacy project) & 3 (team-developed new project)</li>
        <li>MySQL</li>
        <li>PHPUnit & Gitlab CI</li>
        <li>Angular 1 & jQuery</li>
        <li>Twitter Bootstrap</li>
    </ul>

    </p>

    <p>

    <div class="workplace-header">
        <strong>Freelance</strong> <span class="position">Full-stack web developer</span>
        <span class="time-period">2014 - 2017</span>
    </div>

    <ul>
        <li>Landing pages</li>
        <li>Online-shops</li>
        <li>Web applications</li>
    </ul>

    </p>

    <p>

    <div class="workplace-header">
        <strong>The University of Tallinn</strong> <span class="position">Student</span>
        <span class="time-period">2012 - 2014</span>
    </div>

    <ul>
        <li>HTML & CSS</li>
        <li>PHP</li>
        <li>Database architecture</li>
    </ul>

    </p>

    <hr>

    <h3>References</h3>

    <p class="reference">
        <strong>Marko Kallio</strong><span class="phone">+358 400 469 969</span>
        <br>
        <span class="position">Former Suomen Perintätoimisto OY owner, IT director</span>
    </p>

    <p class="reference">
        <strong>Vesa-Matti Lehtinen</strong><span class="phone">+358 401 514 721</span>
        <br>
        <span class="position">Axactor Finland OY, IT director</span>
    </p>

</div>


</body>