## NB!!! 
### Chrome works better, Mozilla leaves a right margin:

<img src="README/CvLayout.png" width="300px"/>

---

### Google Chrome

More settings → Options → Background graphics

<img src="README/ChromePrintDialog.png" width="200px"/>

---

### Firefox Mozilla

More settings → Options → Print backgrounds

<img src="README/MozillaPrintDialog.png" width="200px"/>


## Result

<img src="README/PhilippNikolajev.CV.2022.png" width="200px"/>

[CV pdf](PhilippNikolajev.CV.2022.pdf)
